import React, {useState, useEffect, useContext} from "react";
import DisciplineCard from "../../../components/disciplineCard/DisciplineCard";
import axios from 'axios'
import Grid from "@material-ui/core/Grid";

import {useHistory} from 'react-router-dom'
import {addToken} from "../../../addToken";
import {STUDENT, TOPICS, URL} from "../../../constatnts";
import {userContext} from "../../../App";

import "./Topics.css"

export default function Topics() {
    const {user} = useContext(userContext)
    const history = useHistory();
    const [data, setData] = useState([])

    useEffect(() => {
        if (user) axios.get(URL + STUDENT + TOPICS + `?id=${user.id}`, addToken())
            .then(res => {
                setData(res.data)
            })
            .catch(error => {
                if (error.response.status === 401) history.push("/")
            })
    }, [history, user]);

    return (
        <Grid
            container
            spacing={2}
            justify="center"
            alignItems="stretch"
            direction="column"
            alignContent="center"
        >
            {data.map(item =>
                <Grid item
                      xs={12} sm={8} lg={6} xl={6}>
                    <DisciplineCard
                        item={item}
                        key={item.id}
                    />
                </Grid>
            )
            }
        </Grid>
    )
}