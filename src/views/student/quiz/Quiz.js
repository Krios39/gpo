import React, {useContext, useEffect, useState} from "react";
import {Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button} from "@material-ui/core"
import QuestionCard from "../../../components/questionCard/QuestionCard";

import {quizContext} from "../../../App";
import {useHistory} from 'react-router-dom'

import "./Quiz.css"
import Grid from "@material-ui/core/Grid";

export default function Quiz() {

    const {quiz} = useContext(quizContext)

    const [thisQuiz, setThisQuiz] = useState([])
    const [isDialogOpen, setIsDialogOpen] = useState(false);

    const history = useHistory()


    useEffect(() => {
        setThisQuiz(quiz)
    }, [quiz])

    const answerCheck = () => {
        console.log(thisQuiz)
        // eslint-disable-next-line array-callback-return
        thisQuiz.map(item => {
            if (item.trueAnswer.trim() === "") setIsDialogOpen(true)
        })
    }

    const completeTest = () => {
        setIsDialogOpen(false)
        history.goBack()
    }

    return (
        <div>
            <Grid
                container
                spacing={2}
                justify="center"
                direction="column"
                alignItems="stretch"
                alignContent="center"
            >
                {thisQuiz.map((item, i) =>
                    <Grid
                        item
                        xs={12} sm={8} lg={7} xl={7}>
                        <QuestionCard
                            key={i}
                            item={item}
                            index={i + 1}
                        />
                    </Grid>
                )}
            </Grid>
            <Grid
                container
                spacing={2}
                justify="center"
                direction="column"
                alignItems="center"
                alignContent="center">
                <Button
                    onClick={answerCheck}
                >
                    Завершить тест
                </Button>
            </Grid>
            <Dialog
                open={isDialogOpen}
                onClose={() => setIsDialogOpen(false)}>
                <DialogTitle>{"Вы уверены, что хотите завершить тест?"}</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Не на все вопросы были даны ответы.
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button
                        onClick={completeTest}
                        color="primary"
                        autoFocus>
                        Завершить тест
                    </Button>
                    <Button
                        onClick={() => setIsDialogOpen(false)}
                        color="primary"
                        autoFocus>
                        Продолжить выполение
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}