import React, {useContext, useEffect} from "react";
import {useHistory, Switch, Route} from 'react-router-dom'
import Topics from "./topics/Topics";
import Quiz from "./quiz/Quiz";
import Results from "./results/Results";
import Header from "../../components/header/Header";
import {userContext} from "../../App";
import axios from 'axios'
import {URL, STUDENT, USER} from "../../constatnts";
import {addToken} from "../../addToken";

export default function Student() {

    const {user, setUser} = useContext(userContext)

    const history = useHistory()

    useEffect(() => {
        history.push('/student/topics')
        axios.get(URL + STUDENT + USER + `?id=${localStorage.getItem("USER_ID")}`, addToken())
            .then(res => {
                setUser(res.data)
            })
            .catch(e => {
                history.push('/')
            })
    }, [history, setUser])


    return (
        <div>
            <Header user={user}/>
            <Switch>
                <Route path='/student/topics' component={Topics}/>
                <Route path='/student/quiz' component={Quiz}/>
                <Route path='/student/results' component={Results}/>
            </Switch>
        </div>
    )
}