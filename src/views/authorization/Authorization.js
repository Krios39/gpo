import React, {useContext, useEffect, useState} from "react";
import {TextField, Button, Card,Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions} from "@material-ui/core"
import Loader from 'react-loader-spinner'

import {useHistory} from 'react-router-dom'

import axios from 'axios'

import "./Authorization.css"

import {URL, AUTH} from "../../constatnts";
import {userContext} from "../../App";


export default function Authorization() {

    const {setUser} = useContext(userContext)

    const [open, setOpen] = useState(false);
    const [login, setLogin] = useState("");
    const [password, setPassword] = useState("");
    const [load, setLoad] = useState(false);
    const [errorText, setErrorText] = useState("");

    const history = useHistory();

    useEffect(() => {
        localStorage.clear()
    }, [])

    const handleOpen = text => {
        setLoad(false)
        setErrorText(text);
        setOpen(true);
        setPassword("");
    };

    const check = () => {
        const log = login.trim();
        const pas = password.trim();
        if (login === "" || password === "") handleOpen("НОПИШИ!!!!");
        else pushAuthorization(log, pas);
    };

    const pushAuthorization = (log, pas) => {
        setLoad(true);
        setLogin("");
        setPassword("");
        const authorization = {
            userName: log,
            password: pas,
        };
        axios.post(URL + AUTH, authorization)
            .then(res => {
                setUser(res.data.user);
                setLoad(false);
                localStorage.setItem('ACCESS_TOKEN', res.data.accessToken);
                localStorage.setItem('USER_ID', res.data.user.id);
                 history.push("/student/topics")
            })
            .catch(error => {
                if (error.response.status === 401) handleOpen("ЧОТ НЕ ТО ВВЁЛ")
            })
    };

    return (
        <div className="box">
            <Card className="authorizationBox">
                <div className="title">ТЕСТИКИ</div>
                <div className="loginBox">
                    <TextField
                        variant="outlined"
                        label="Введите логин"
                        value={login}
                        onChange={event => setLogin(event.target.value)}/>
                </div>
                <div>
                    <div className="loginBox">
                        <TextField
                            variant="outlined"
                            label="Введите пароль"
                            type="password"
                            value={password}
                            onChange={event => setPassword(event.target.value)}/>
                    </div>
                </div>
                <div className="buttonBox">
                    <div className="loaderBox"><Loader
                        visible={load}
                        type="TailSpin"
                        height={60}
                        width={60}
                    /></div>
                    {!load && <Button
                        variant="outlined"
                        color="primary"
                        onClick={check}>
                        Войти
                    </Button>}

                </div>
            </Card>

            <Dialog
                open={open}
                onClose={()=>setOpen(false)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description">
                <DialogTitle>{"Не сдано"}</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        {errorText}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button
                        onClick={()=>setOpen(false)}
                        color="primary"
                        autoFocus>
                        Не понял
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}