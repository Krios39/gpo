import React, {createContext, useState} from 'react';
import Authorization from "./views/authorization/Authorization";
import {Switch, Route} from 'react-router-dom'
import Student from "./views/student/Student";
import Teacher from "./views/teacher/Teacher";

export const userContext = createContext(undefined, undefined)
export const quizContext = createContext(undefined, undefined)

export default function App() {

    const [user, setUser] = useState()
    const [quiz, setQuiz] = useState([])


    return (
        <userContext.Provider value={{user, setUser}}>
            <Switch>
                <Route exact path='/' component={Authorization}/>
                <quizContext.Provider value={{quiz, setQuiz}}>
                    <Route path='/student' component={Student}/>
                </quizContext.Provider>
                <Route path='/teacher' component={Teacher}/>
            </Switch>
        </userContext.Provider>
    );
}

