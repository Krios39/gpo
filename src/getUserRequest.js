import axios from "axios";
import {STUDENT, URL, USER} from "./constatnts";
import {addToken} from "./addToken";

export const getUserRequest = () => {
    axios.get(URL + STUDENT + USER + `?id=${localStorage.getItem("USER_ID")}`, addToken())
        .then(res => {
            console.log("Пользователь получен", res.data);
            return res.data
        })
        .catch(e => {
            return null
        })

}