import React, {useContext, useEffect, useState} from "react";
import {Button} from "@material-ui/core";
import axios from 'axios'

import {OPTION, STUDENT, URL} from "../../constatnts";
import {quizContext, userContext} from "../../App";
import {addToken} from "../../addToken";
import {useHistory} from "react-router-dom";

import "./TopicCard.css"
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

export default function TopicCard(topic) {
    const history = useHistory();
    const {setQuiz} = useContext(quizContext)
    const {user} = useContext(userContext)
    const [thisTopic, setThisTopics] = useState({})

    useEffect(() => {
        setThisTopics(topic.topic)
    }, [topic.topic])

    const startTest = () => {
        console.log("Начат тест по ", thisTopic)
        axios.get(URL + STUDENT + OPTION + `?user_id=${user.id}&topic_id=${thisTopic.id}`, addToken())
            .then(req => {
                setQuiz(req.data)
                history.push("/student/quiz")
            })
            .catch(e => {
                console.log(e)
            })
    }

    return (
        <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center"
            wrap="nowrap"
        >
            <Grid item zeroMinWidth>
                <div className="title">
                    <Typography variant="h6" noWrap>
                        {thisTopic.name}
                    </Typography>
                </div>
            </Grid>
            <Grid
                item
                alignItems="center"
                direction="column"
                justify="center"
            >
                <Grid
                    container
                    direction="column"
                    justify="center"
                    alignItems="center"
                >
                    <div className="button">
                        <Button
                            size="medium"
                            className="button"
                            // onClick={startTest}
                        >
                            Ознакомление
                        </Button>
                    </div>
                    <div className="button">
                        <Button
                            size="medium"
                            className="button"
                            onClick={startTest}
                        >
                            Начать тест
                        </Button>
                    </div>
                </Grid>
            </Grid>
        </Grid>
    )
}