import React, {createContext, useEffect, useState} from "react";
import {Input, InputAdornment} from "@material-ui/core";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import IconButton from "@material-ui/core/IconButton";
import ArrowDropDown from "@material-ui/icons/ArrowDropDown"
import FormHelperText from "@material-ui/core/FormHelperText";
import MathConstructor from "../mathConstructor/MathConstructor";
import Paper from "@material-ui/core/Paper";
import "./QuestionCard.css"

export default function QuestionCard({item, index}) {
    const [thisQuestion, setThisQuestion] = useState()
    const [answer, setAnswer] = useState(" ")
    const [isError, setError] = useState(false)
    const [errorLabel, setErrorLabel] = useState("")
    const [helperText, setHelperText] = useState("")

    useEffect(() => {
        setThisQuestion(item.question)
    }, [item])

    useEffect(() => {
        item.trueAnswer = answer
        if (answer === "") {
            fieldErrorDeclaration()
        }
    }, [answer, item.trueAnswer])

    const fieldErrorDeclaration = () => {
        setErrorLabel("Поле пустое")
        setHelperText("Введите ответ")
        setError(true)
    }

    const textFieldChange = (event) => {
        setAnswer(event.target.value.trim())
        setErrorLabel("")
        setHelperText("")
        setError(false)
    }

    return (
        <Card className="questionCard">
            <div className="question">
                {index}. {thisQuestion}
            </div>
            <TextField
                error={isError}
                label={errorLabel}
                helperText={helperText}
                value={answer}
                onChange={event => textFieldChange(event)}
                fullWidth
            />
        </Card>
    )
}