import React, {useEffect, useState} from "react";
import TopicCard from "../topicCard/TopicCard";

import "./DisciplineCard.css"
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";

export default function DisciplineCard({item}) {
    const [discipline, setDiscipline] = useState({})
    const [topics, setTopics] = useState([])

    useEffect(() => {
        setDiscipline(item)
        setTopics([...item.topics])
    }, [item])

    const [expanded, setExpanded] = React.useState(false);

    const handleChange = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    return (
        <Paper>
            <ExpansionPanel expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
                <ExpansionPanelSummary>
                    <div className="heading">
                        {discipline.name}
                    </div>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Grid
                        container
                        spacing={1}
                        justify="center"
                        alignItems="center"
                    >
                        {topics.map(topic =>
                            <Grid item xs={12} lg={12}>
                                <Paper>
                                    <TopicCard
                                        key={topic.id}
                                        topic={topic}
                                    />
                                </Paper>
                            </Grid>
                        )}
                    </Grid>
                </ExpansionPanelDetails>
            </ExpansionPanel>
        </Paper>
    )

}