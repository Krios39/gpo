import AppBar from "@material-ui/core/AppBar";
import React, {useEffect, useRef, useState} from 'react';
import {
    Button,
    ClickAwayListener,
    Grow,
    Paper,
    Popper,
    MenuItem,
    MenuList,
    Typography,
    Toolbar
} from '@material-ui/core';

import "./Header.css"
import {makeStyles} from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";

export default function Header({user}) {

    const [username, setUsername] = useState("");
    const [open, setOpen] = useState(false);
    const anchorRef = useRef(null);
    const [screenSize, setScreenSize] = useState('')

    const handleToggle = () => {
        setOpen((prevOpen) => !prevOpen);
    };

    const handleClose = (event) => {
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
            return;
        }

        setOpen(false);
    };

    function handleListKeyDown(event) {
        if (event.key === 'Tab') {
            event.preventDefault();
            setOpen(false);
        }
    }

    const prevOpen = React.useRef(open);
    useEffect(() => {
        if (prevOpen.current === true && open === false) {
            anchorRef.current.focus();
        }
        prevOpen.current = open;
    }, [open]);

    useEffect(() => {
        if (user) setUsername(user.name)
    }, [user])


    useEffect(() => {
        // eslint-disable-next-line no-restricted-globals
        const thisScreenWidth = screen.width
        if (thisScreenWidth < 500) setScreenSize('100%')
        else setScreenSize('50%')
    }, [])

    const useStyles = makeStyles(() => ({
        root: {
            width: screenSize,
        },
    }));

    const classes = useStyles();

    return (
        <div className="headerBox">
        {/*//     <div className={classes.root}>*/}
        <Grid
            container
            justify="center"
            alignContent="center"
            alignItems="center"
        >
            <Grid
                item
                xs={12} sm={8} lg={6} xl={6}>
                <AppBar color="inherit" position="static">
                    <Toolbar>
                        <div className="toolbarBox">
                            <div className="nameBox">
                                <Typography>{username}</Typography>
                            </div>
                            <div className="headerButtonBox">
                                <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition
                                        disablePortal>
                                    {({TransitionProps, placement}) => (
                                        <Grow
                                            {...TransitionProps}
                                            style={{transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom'}}
                                        >
                                            <Paper>
                                                <ClickAwayListener onClickAway={handleClose}>
                                                    <MenuList autoFocusItem={open} id="menu-list-grow"
                                                              onKeyDown={handleListKeyDown}>
                                                        <MenuItem onClick={handleClose}>Тестирования</MenuItem>
                                                        <MenuItem onClick={handleClose}>Результаты</MenuItem>
                                                    </MenuList>
                                                </ClickAwayListener>
                                            </Paper>
                                        </Grow>
                                    )}
                                </Popper>

                                <Button
                                    ref={anchorRef}
                                    aria-controls={open ? 'menu-list-grow' : undefined}
                                    aria-haspopup="true"
                                    onClick={handleToggle}
                                >
                                    Менюшка
                                </Button>
                            </div>
                        </div>
                    </Toolbar>
                </AppBar>
            </Grid>
        </Grid>
     </div>
        // </div>
    );
}

// import React from 'react';
// import { makeStyles } from '@material-ui/core/styles';
// import AppBar from '@material-ui/core/AppBar';
// import Toolbar from '@material-ui/core/Toolbar';
// import Typography from '@material-ui/core/Typography';
// import Button from '@material-ui/core/Button';
// import IconButton from '@material-ui/core/IconButton';
//
// const useStyles = makeStyles((theme) => ({
//     root: {
//         display:"flex",
//         flexGrow: 1,
//         width:"50%",
//         justifyContent:"normal",
//     },
//
// }));
//
// export default function Header() {
//     const classes = useStyles();
//
//     return (
//         <div className={classes.root}>
//             <AppBar position="static">
//                 <Toolbar>
//                     <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
//
//                     </IconButton>
//                     <Typography variant="h6" className={classes.title}>
//                         News
//                     </Typography>
//                     <Button color="inherit">Login</Button>
//                 </Toolbar>
//             </AppBar>
//         </div>
//     );
// }
