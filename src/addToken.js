export const addToken = () => {                                             //Функия добавляет в объект header с токеном
    const headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('ACCESS_TOKEN')
    }
    const defaults = {headers: headers};
    return Object.assign({}, defaults)
}